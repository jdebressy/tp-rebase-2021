
# GitLab



## Démarrage

On utilise l'interface graphique de Gitlab pour créer un fork du projet.  
On ajoute ensuite le remote avec   
`git remote add personal https://gitlab.com/jdebressy/tp-rebase-2021.git`  
On utilise de nouveau l'interface graphique pour ajouter un nouveau membre au projet.  


## Votre branche

On crée une issue via l'interface web.  
On crée une nouvelle branche avec  
`git checkout -b 1-customize-readme`  
Après avoir modifié le contenu de README, on ajoute le fichier comme d'habitude avec  
`git add README.md`  
`git commit -m "modification du readme"`  
On push ensuite sur origin avec  
`git push -u origin 1-customize-readme`  
Il faut s'identifier. On utilise l'identifiant gitlab et un access token créé sur l'interface web, avec les droits read_repository et write_repository.

On a finalement une erreur 403. Le repo origin ne nous appartient pas et nous n'y avons pas de droit d'accès.

On push alors sur personal avec  
`git push -u personal 1-customize-readme`  
en renseignant également ses identifiants.

> Suite à une erreur d'inattention, j'ai push ma branche sur `main`, mais cela ne semble pas poser de problème pour la suite.


## Merge request

On crée la merge request via l'interface web. On sélectionne `jdebressy/1-customize-readme` comme branche source et `jdebressy/main` en branche cible.  
Pour assigner la merge request à un autre membre, on l'ajoute d'abord au projet avec un statut lui donnant des droits d'édition, puis on l'assigne sur le menu de création de la merge request.  
Après avoir commenté et approuvé la merge request, on constate que les modifications sont bien visibles sur le README. On constate que l'issue est bien marquée comme résolue.  
On met à jour en local avec  
`git checkout main`  
`git pull personal main`


## Rapport